
`timescale 1 ns / 1 ps

	module clictd_basic_readout_v1_0 #
	(
		// Users to add parameters here
    parameter integer OUTPUT_BYTES = 4,
    parameter integer USE_CHIPSCOPE1 = 1,
    parameter integer USE_CHIPSCOPE2 = 1,
    
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(
		// Users to add ports here
    input wire chip_clk,
    input wire chip_data,
    input wire chip_enable,
    output wire chip_readout,
    output wire chip_pwren,
    output wire chip_tpulse,
    output wire chip_shutter,
    output wire chip_rstn,
    
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
	
  wire rst_sync_chip;
  wire rst_sync_axi;
  wire arst;
  
  wire chip_clk_i;
  wire chip_clkn;
  wire chip_data_i;
  wire chip_enable_i;
  wire chip_readout_i;
  wire chip_pwren_i;
  wire chip_tpulse_i;
  wire chip_shutter_i;
  wire chip_rstn_i;

  
  wire [(OUTPUT_BYTES*8)-1 : 0] fifo_data_out;
  wire [(OUTPUT_BYTES*8)-1 : 0] fifo_data_in;
  wire fifo_data_in_valid;
  wire fifo_data_in_write;
  wire fifo_data_out_valid;
  wire fifo_data_out_read;
  wire fifo_empty;
  
  wire rd_start_sync_axi;
  wire rd_start_sync_chip;
  
  wire rd_reset;
  
  wire [15:0] frame_bitcount;
  wire [7:0] data_byte;
  wire data_byte_valid;
  wire data_byte_end_sync_axi;
  wire data_byte_end_sync_chip;
  wire data_readout_end;
  
  wire chip_clk_rise;
  wire chip_clk_fall;
  wire chip_clk_miss;


  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) reg chip_data_sync [2:0];
  wire chip_data_sync_wire;
  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) reg chip_enable_sync [2:0];
  wire chip_enable_sync_wire;
	
	
// Instantiation of Axi Bus Interface S00_AXI
	clictd_basic_readout_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) clictd_basic_readout_v1_0_S00_AXI_inst (
	
			// Users to add ports here
      .fifo_data_out(fifo_data_out),
      .fifo_data_out_valid(fifo_data_out_valid),
      .fifo_data_read(fifo_data_out_read),
      .rd_end(data_byte_end_sync_axi),
      .rd_start(rd_start_sync_axi),
      .rd_reset(rd_reset),
      
      .chip_pwren(chip_pwren_i),
      .chip_tpulse(chip_tpulse_i),
      .chip_shutter(chip_shutter_i),
      .chip_rstn(chip_rstn_i),
      
      .chip_clk_rise(chip_clk_rise),
      .chip_clk_fall(chip_clk_fall),
      .chip_clk_miss(chip_clk_miss),
      
	
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);

	// Add user logic here
	
	
	assign arst = !(s00_axi_aresetn) || rd_reset;
	//assign chip_clk_i = ~chip_clkn;
	assign fifo_data_out_valid = !fifo_empty;
	
  // capture input synchronously to chip_clk
  assign chip_data_i   = chip_data_sync[2];
  assign chip_enable_i = chip_enable_sync[2];
  always @(posedge chip_clk) begin
    chip_data_sync[2]   <= chip_data_sync[1];
    chip_enable_sync[2] <= chip_enable_sync[1];
  end
  
  always @(negedge chip_clk) begin
    chip_data_sync[1]   <= chip_data_sync[0];
    chip_data_sync[0]   <= chip_data_sync_wire;
    chip_enable_sync[1] <= chip_enable_sync[0];
    chip_enable_sync[0] <= chip_enable_sync_wire;
  end

  // Synchronise reset to chip_clk
  sync_reset #(
    .NEGATIVE(1'b0),
    .STAGES(2) 
  ) datarx_rst_chip (
    .clk(chip_clk),
    .arst_in(arst),  // i, async 
    .rst_out(rst_sync_chip)   // o, sync to clk, active 1
  );
  // Synchronise reset to axi_clk
  sync_reset #(
    .NEGATIVE(1'b0),
    .STAGES(2) 
  ) datarx_rst_axi (
    .clk(s00_axi_aclk),
    .arst_in(arst),  // i, async 
    .rst_out(rst_sync_axi)   // o, sync to clk, active 1
  );
  
  sync_signal #(
      .WIDTH(1),
      .STAGES(2) 
  ) axi_to_chip (
      .out_clk(chip_clk),
      .signal_in(rd_start_sync_axi),  // i, async 
      .signal_out(rd_start_sync_chip)  // o, sync to out_clk
  );
  
  sync_signal #(
      .WIDTH(1),
      .STAGES(2) 
  ) chip_to_axi (
      .out_clk(s00_axi_aclk),
      .signal_in(data_byte_end_sync_chip),  // i, async 
      .signal_out(data_byte_end_sync_axi)  // o, sync to out_clk
  );

    assign chip_clk_i = chip_clk;
    assign chip_data_sync_wire = chip_data;
    assign chip_enable_sync_wire = chip_enable;
    assign chip_readout = chip_readout_i;
    assign chip_pwren = chip_pwren_i;
    assign chip_tpulse = chip_tpulse_i;
    assign chip_shutter = chip_shutter_i;
    assign chip_rstn = chip_rstn_i;


  clkcheck clkcheck_inst(
    .clk40(chip_clk_i),
    .clk250(s00_axi_aclk),
    .reset(rst_sync_axi),
    .rise(chip_clk_rise),
    .fall(chip_clk_fall),
    .noclk(chip_clk_miss)
  );

  clictd_datarx clictd_datarx_inst (
    .rst(rst_sync_chip),
    .chip_clk(chip_clk),
    .chip_data(chip_data_i),
    .chip_enable(chip_enable_i),
    .readout_start(rd_start_sync_chip),
    .chip_readout(chip_readout_i),
    .readout_end(data_byte_end_sync_chip),
    .data_out(data_byte),
    .data_valid(data_byte_valid),
    .frame_bitcount(frame_bitcount)
  );

  clictd_bytealign #(
    .OUTPUT_BYTES(4)
  ) clictd_bytealign_inst (
    .rst(rst_sync_chip),
    .clk(chip_clk),
    .byte_in(data_byte),
    .datain_valid(data_byte_valid),
    .chip_readout(chip_readout_i),
    .datain_end(data_byte_end_sync_chip),
    .data_out(fifo_data_in),
    .dataout_valid(fifo_data_in_valid),
    .dataout_end(data_readout_end)
  );

  readout_fifo readout_fifo_inst (
    .rst(arst),        // input wire rst
    .wr_clk(chip_clk),  // input wire wr_clk
    .rd_clk(s00_axi_aclk),  // input wire rd_clk
    .din(fifo_data_in),        // input wire [31 : 0] din
    .wr_en(fifo_data_in_valid),    // input wire wr_en
    .rd_en(fifo_data_out_read),    // input wire rd_en
    .dout(fifo_data_out),      // output wire [31 : 0] dout
    .full(),      // output wire full
    .empty(fifo_empty)    // output wire empty
  );

  generate
    if( USE_CHIPSCOPE1 == 1) begin : chipscope1
      clictd_readout_ila readout_ila_inst (
        .clk(chip_clk), // input wire clk
      
      
        .probe0(data_byte), // input wire [7:0]  probe0  
        .probe1(data_byte_valid), // input wire [0:0]  probe1 
        .probe2(fifo_data_in), // input wire [31:0]  probe2 
        .probe3(fifo_data_in_valid), // input wire [0:0]  probe3 
        .probe4(chip_data_i), // input wire [0:0]  probe4 
        .probe5(chip_enable_i), // input wire [0:0]  probe5 
        .probe6(chip_readout_i), // input wire [0:0]  probe6 
        .probe7(rd_start_sync_chip), // input wire [0:0]  probe7 
        .probe8(data_byte_end_sync_chip), // input wire [0:0]  probe8 
        .probe9(data_readout_end), // input wire [0:0]  probe9
        .probe10(frame_bitcount) // input wire [15:0]  probe10
      );

    end
    if( USE_CHIPSCOPE2 == 1 ) begin : chipscope2
    ila_0 axi_ila_inst (
        .clk(s00_axi_aclk), // input wire clk
        
        .probe0(chip_clk_rise), // input wire [0:0]  probe
        .probe1(chip_clk_fall), // input wire [0:0]  probe
        .probe2(chip_clk_miss), // input wire [0:0]  probe
        .probe3(chip_clk_i) // input wire [0:0]  probe
    
      );
      
    end
  endgenerate

	// User logic ends

	endmodule
