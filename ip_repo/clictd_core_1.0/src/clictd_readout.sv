`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2019 15:13:41
// Design Name: 
// Module Name: clictd_readout
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


  module clictd_readout #(
    OUTPUT_BYTES = 4
  )(
    // clk clock domain
    input  fifo_clk,
    input  rst,
    output [(OUTPUT_BYTES*8)-1 : 0] fifo_data_out,
    output fifo_data_out_valid,
    input  fifo_data_read,
    
    // chip_clk clock domain    
    output [15:0] frame_bitcount,
    output data_readout_end,
    output fifo_ovf,
    output readout_idle,
    input  readout_start,
    
    input  chip_clk,
    input  chip_data,
    input  chip_enable,

    output chip_readout
  );
    
  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) logic chip_data_sync [2:0];
  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) logic chip_enable_sync [2:0];
  logic chip_data_i;
  logic chip_enable_i;
  logic chip_readout_i;
  
  logic fifo_full;
  logic fifo_ovf_latch;
  logic data_byte_end;
  logic [7:0] data_byte;
  logic data_byte_valid;
  logic [(OUTPUT_BYTES*8)-1 : 0] fifo_data_in;
  logic fifo_data_in_valid;
  logic fifo_data_in_write;
  logic fifo_empty;
  
  //assign readout_active = chip_readout_i || data_byte_end;
  assign fifo_data_out_valid = !fifo_empty;
  assign chip_readout = chip_readout_i;
  assign fifo_ovf = fifo_ovf_latch;
    
  // capture input from chip synchronously to chip_clk
  assign chip_data_i   = chip_data_sync[2];
  assign chip_enable_i = chip_enable_sync[2];
  //
  always @(posedge chip_clk) begin
    chip_data_sync[2]   <= chip_data_sync[1];
    chip_enable_sync[2] <= chip_enable_sync[1];
  end
  //
  always @(negedge chip_clk) begin
    chip_data_sync[1]   <= chip_data_sync[0];
    chip_data_sync[0]   <= chip_data;
    chip_enable_sync[1] <= chip_enable_sync[0];
    chip_enable_sync[0] <= chip_enable;
  end

  clictd_datarx clictd_datarx_inst (
    .rst(rst),
    .chip_clk(chip_clk),
    .chip_data(chip_data_i),
    .chip_enable(chip_enable_i),
    .readout_start(readout_start),
    .chip_readout(chip_readout_i),
    .readout_end(data_byte_end),
    .readout_idle(readout_idle),
    .data_out(data_byte),
    .data_valid(data_byte_valid),
    .frame_bitcount(frame_bitcount)
  );

  clictd_bytealign #(
    .OUTPUT_BYTES(OUTPUT_BYTES)
  ) clictd_bytealign_inst (
    .rst(rst),
    .clk(chip_clk),
    .byte_in(data_byte),
    .datain_valid(data_byte_valid),
    .chip_readout(chip_readout_i),
    .datain_end(data_byte_end),
    .data_out(fifo_data_in),
    .dataout_valid(fifo_data_in_valid),
    .dataout_end(data_readout_end)
  );

  always_ff @(posedge chip_clk) begin
    if (rst || readout_start)
      fifo_ovf_latch <= 1'b0;
    else if (fifo_data_in_valid && fifo_full)
      fifo_ovf_latch <= 1'b1;
  end

  readout_fifo readout_fifo_inst (
    .rst(rst),        // input wire rst
    .wr_clk(chip_clk),  // input wire wr_clk
    .rd_clk(fifo_clk),  // input wire rd_clk
    .din(fifo_data_in),        // input wire [31 : 0] din
    .wr_en(fifo_data_in_valid),    // input wire wr_en
    .rd_en(fifo_data_read),    // input wire rd_en
    .dout(fifo_data_out),      // output wire [31 : 0] dout
    .full(fifo_full),      // output wire full
    .empty(fifo_empty)    // output wire empty
  );

  
endmodule
