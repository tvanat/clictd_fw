`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2019 20:35:19
// Design Name: 
// Module Name: data_collector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module data_collector #(
  )(
    input clk,
    input ,
    input ,
    input ,
    input ,
    
 );


  always_comb begin   
    // default is to stay in current state
    state_next = state;
    case (state)
      S_IDLE : begin
        if (c_start)
          state_next = S_LATCH_COUNTERS;
        else
          state_next = S_IDLE;
      end

      S_LATCH_COUNTERS : begin
        state_next = S_LATCH_SUM;
      end

      S_LATCH_SUM : begin
        state_next = S_READ_TS;
      end

      S_READ_TS : begin
        if (!ts_data_count_zero)
          state_next = S_READ_TS;
        else 
          state_next = S_READ_DATA;
      end

      S_READ_DATA : begin
        if (!ts_data_count_zero)
          state_next = S_READ_DATA;
        else 
          state_next = S_FINISH;
      end

      FINISH : begin
          state_next = S_IDLE;

      default : begin
        state_next = S_IDLE;
      end
    endcase  
  end
  
endmodule
