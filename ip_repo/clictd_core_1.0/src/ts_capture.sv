`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// signal clock domain crossing 
module ts_capture #(
    byte unsigned TS_WIDTH = 48,
    byte unsigned TS_SIGNALS_N = 16 
  )(
    input  clk,
    input  rst,
    input  t0,
    input  count_enable,
    input  capture_enable,
    input  [TS_WIDTH-1  : 0] conf_ts_init_value,
    input  [(TS_SIGNALS_N*2)-1 : 0] conf_signal_edges,
    input  [TS_SIGNALS_N-1 : 0] signals_in,
    output [TS_WIDTH-1  : 0] timestamp_out,
    output [TS_SIGNALS_N-1 : 0] signals_out,
    output [TS_SIGNALS_N-1 : 0] signals_trig_out,
    output output_valid
  );

  logic [TS_WIDTH-1 : 0] timestamp_cnt;
  logic [TS_WIDTH-1 : 0] timestamp_capture;
  logic [TS_SIGNALS_N-1 : 0] signals_prev;
  logic [TS_SIGNALS_N-1 : 0] signals_capture;
  logic [TS_SIGNALS_N-1 : 0] signals_trig_capture;
  logic output_valid_r;
  logic capture;
  logic [TS_SIGNALS_N-1 : 0] trigger_signals;

  assign output_valid     = output_valid_r;
  assign timestamp_out    = timestamp_capture;
  assign signals_out      = signals_capture;
  assign signals_trig_out = signals_trig_capture;
  

  always_ff @(posedge clk) begin
    signals_prev <= signals_in;
  end

  always_ff @(posedge clk) begin
    if (rst || t0) begin
      timestamp_cnt <= conf_ts_init_value;
    end
    else if (count_enable) begin
      timestamp_cnt <= timestamp_cnt + 1;
    end 
    else begin
      timestamp_cnt <= timestamp_cnt;
    end
  end

  always_ff @(posedge clk) begin
    if (rst) begin
      timestamp_capture    <= 'b0;
      signals_capture      <= 'b0;
      signals_trig_capture <= 'b0;
      output_valid_r       <= 'b0;
    end
    else if (capture && capture_enable) begin
      timestamp_capture    <= timestamp_cnt;
      signals_trig_capture <= trigger_signals;
      signals_capture      <= signals_in;
      output_valid_r       <= 'b1;
    end 
    else begin
      timestamp_capture    <= timestamp_capture;
      signals_capture      <= signals_capture;
      signals_trig_capture <= signals_trig_capture;
      output_valid_r       <= 'b0;
    end
  end  

  generate
    // individual triggers based on their settings
    for (genvar i=0; i < TS_SIGNALS_N; i=i+1) begin
      always_comb begin
        case (conf_signal_edges[(2*i)+1 : (2*i)])
          // None
          2'b00 : begin
            trigger_signals[i] = 1'b0;
          end
  
          // Rising
          2'b01 : begin
            if (~signals_prev[i] & signals_in[i])
              trigger_signals[i] = 1'b1;
            else
              trigger_signals[i] = 1'b0;
          end
  
          // Falling
          2'b10 : begin
            if (signals_prev[i] & ~signals_in[i])
              trigger_signals[i] = 1'b1;
            else
              trigger_signals[i] = 1'b0;
          end
  
          // Both
          2'b11 : begin
            if (signals_prev[i] ^ signals_in[i])
              trigger_signals[i] = 1'b1;
            else
              trigger_signals[i] = 1'b0;
          end
  
          default : begin
            trigger_signals[i] = 1'b0;
          end

        endcase
      end
    end
  endgenerate

  assign capture = |trigger_signals;

endmodule