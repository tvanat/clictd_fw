`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 09:48:04
// Design Name: 
// Module Name: caribou_clictd_datarx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// NEGATIVE CLOCK EDGE IS ACTIVE corresponding with CLICTD output
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// 
//////////////////////////////////////////////////////////////////////////////////


module clictd_bytealign #(
  byte unsigned OUTPUT_BYTES = 4
)(
  input rst,
  input clk,
  input [7 : 0] byte_in,
  input datain_valid,
  input chip_readout,
  input datain_end,
  output [(OUTPUT_BYTES*8)-1 : 0] data_out,
  output dataout_valid,
  output dataout_end
);

  generate
    if( OUTPUT_BYTES == 1) begin : no_extension
      assign data_out      = byte_in;
      assign dataout_valid = datain_valid;
      assign dataout_end   = datain_end;
    end
    else begin : extend
      
      logic dataout_end_i;
      logic dataout_valid_i;
      logic chip_readout_prev;
      logic chip_readout_start;
      logic [OUTPUT_BYTES-1 : 0] bytepointer;
      logic [OUTPUT_BYTES-1 : 0] bytemask;
      logic [OUTPUT_BYTES-1 : 0] byte_wren;
      logic [(OUTPUT_BYTES*8)-1 : 0] data_wide;
      
      assign dataout_end = dataout_end_i;
      assign dataout_valid = dataout_valid_i;
      
      always_ff @(posedge clk) begin
        chip_readout_prev <= chip_readout;
      end;
      assign chip_readout_start = !chip_readout_prev && chip_readout;
      
      always_ff @(posedge clk) begin
        if (rst || chip_readout_start) begin
          // set MSB active
          bytepointer[OUTPUT_BYTES-1] <= 1'b1;
          bytepointer [OUTPUT_BYTES-2 : 0] <= 'b0;
        end
        else if (datain_valid) begin
          // rotate right
          bytepointer[OUTPUT_BYTES-1] <= bytepointer[0];
          bytepointer [OUTPUT_BYTES-2 : 0] <= bytepointer [OUTPUT_BYTES-1 : 1];
        end
        else begin
          // keep 
          bytepointer <= bytepointer;
        end
      end
      
      // data_wide demux
      for (genvar i=0; i < OUTPUT_BYTES; i=i+1) begin
        assign bytemask[i] = (|bytepointer[OUTPUT_BYTES-1 : i]);
        assign byte_wren[i] = bytemask[i] && datain_valid;

        always_ff @(posedge clk) begin     
          if (byte_wren[i]) begin
            // to be written...
            if (bytepointer[i]) begin
              // ...with input data
              data_wide[((i+1)*8)-1 : i*8] <= byte_in;
            end
            else begin
              // ...with zeros
              data_wide[((i+1)*8)-1 : i*8] <= 'b0;
            end
          end
          else begin
            // keep
            data_wide[((i+1)*8)-1 : i*8] <= data_wide[((i+1)*8)-1 : i*8];
          end
        end
      end // for loop
      // demux

      assign data_out = data_wide;

      always_ff @(posedge clk) begin
        if (rst || chip_readout_start) begin
          dataout_end_i   <= 1'b0;
          dataout_valid_i <= 1'b0;
        end
        else begin 
          if (datain_valid && (bytepointer[0] || datain_end)) begin
            dataout_valid_i <= 1'b1;
            dataout_end_i   <= datain_end;
          end
          else begin
            dataout_valid_i <= 1'b0;
            dataout_end_i   <= 1'b0;
          end
        end
      end

    end
  endgenerate

endmodule  