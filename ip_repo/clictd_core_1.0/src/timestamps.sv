`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// signal clock domain crossing 
module timestamps #(
    byte unsigned TS_WIDTH = 48,
    byte unsigned TS_SIGNALS_N = 8
  )(
    input  clk_ts,
    input  clk_cfg,
    input  rst,
    input  t0,
    input  get_ts_count,
    input  conf_enable,
    input  conf_capture_enable,
    input  [TS_WIDTH-1  : 0] conf_ts_init_value,
    input  [(TS_SIGNALS_N*2)-1 : 0] conf_signal_edges,
    input  [TS_SIGNALS_N-1 : 0] signals_in,
    input  read_ts_fifo,
    output [((2*TS_SIGNALS_N)+TS_WIDTH)-1  : 0] ts_fifo_out,
    output ts_count_valid,
    output [23:0] ts_count,
    output ts_fifo_ovf,
    output output_valid
  );

  logic [TS_WIDTH-1  : 0] conf_ts_init_value_Sts;
  logic [(TS_SIGNALS_N*2)-1 : 0] conf_signal_edges_Sts;
  logic conf_enable_Sts;
  logic conf_capture_enable_Sts;
  logic [TS_WIDTH-1 : 0] timestamp_store;
  logic [TS_SIGNALS_N-1 : 0] signals_store;
  logic [TS_SIGNALS_N-1 : 0] signals_trig_store;
  logic store_event;
  logic ts_valid;
  logic fifo_empty;
  logic ts_fifo_ovf_latch_i;
  logic ts_fifo_ovf_i;
  //logic [TS_SIGNALS_N-1 : 0] trigger_signals;
  logic [((2*TS_SIGNALS_N)+TS_WIDTH)-1 : 0] fifo_data_in;
  logic [23:0] ts_count_int;
  logic [23:0] ts_count_latch;
  logic get_ts_count_Scfg;
  logic ts_count_valid_Scfg;
  
  assign output_valid = ~fifo_empty;

  sync_bus #(
      .WIDTH(TS_WIDTH),
      .STAGES(2)
  ) sync_cfg_init_val (
      .in_clk(clk_cfg),
      .out_clk(clk_ts),
      .bus_in(conf_ts_init_value),
      .bus_out(conf_ts_init_value_Sts)
  );
  
  sync_bus #(
      .WIDTH(TS_SIGNALS_N*2),
      .STAGES(2)
  ) sync_cfg_signal_edges (
      .in_clk(clk_cfg),
      .out_clk(clk_ts),
      .bus_in(conf_signal_edges),
      .bus_out(conf_signal_edges_Sts)
  );
  
  sync_signal #(
      .WIDTH(2),
      .STAGES(2)
  ) sync_level_cfg (
      .out_clk(clk_ts),
      .signal_in({conf_enable, conf_capture_enable}),
      .signal_out({conf_enable_Sts, conf_capture_enable_Sts})
  );

  ts_capture #(
    .TS_WIDTH (48),
    .TS_SIGNALS_N(TS_SIGNALS_N) 
  ) ts_capture_inst (
    .clk(clk_ts),
    .rst(rst),
    .t0(t0),
    .count_enable(conf_enable_Sts),
    .capture_enable(conf_capture_enable_Sts && conf_enable_Sts),
    .conf_ts_init_value(conf_ts_init_value_Sts),
    .conf_signal_edges(conf_signal_edges_Sts),
    .signals_in(signals_in),
    .timestamp_out(timestamp_store),
    .signals_out(signals_store),
    .signals_trig_out(signals_trig_store),
    .output_valid(ts_valid)
  );
  
  sync_pulse #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_get_ts_count (
      .in_clk(clk_cfg),
      .out_clk(clk_ts),
      .pulse_in(get_ts_count),
      .pulse_out(get_ts_count_Scfg)
  ); 
  sync_pulse #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_ts_count_valid (
      .in_clk(clk_ts),
      .out_clk(clk_cfg),
      .pulse_in(ts_count_valid_Scfg),
      .pulse_out(ts_count_valid)
  ); 
  
  assign ts_count    = ts_count_latch;
  assign ts_fifo_ovf = ts_fifo_ovf_i;
  
  always_ff @(posedge clk_ts) begin
    if (rst) begin
      ts_count_int         <= 24'b0;
      ts_count_latch       <= 24'b0;
      ts_count_valid_Scfg  <= 1'b0;
      ts_fifo_ovf_i        <= 1'b0;
    end
    else begin
      if (get_ts_count_Scfg) begin
        ts_count_latch       <= ts_count_int;
        ts_count_valid_Scfg  <= 1'b1;
        ts_fifo_ovf_i        <= ts_fifo_ovf_latch_i;
        if (store_event)
          ts_count_int <= 24'd1;
        else 
          ts_count_int <= 24'b0;
      end
      else begin 
        ts_count_valid_Scfg <= 1'b0;
        if (store_event)
          ts_count_int <= ts_count_int + 1;
      end
    end
  end
  
  always_ff @(posedge clk_ts) begin
    if (rst) begin 
      ts_fifo_ovf_latch_i <= 1'b0;
    end
    else begin
      if (ts_valid && ts_fifo_full) begin
        ts_fifo_ovf_latch_i <= 1'b1;
      end
      else if (ts_count_valid_Scfg) begin
        ts_fifo_ovf_latch_i <= 1'b0;
      end
    end
  end
  
  
  assign store_event = ts_valid && !ts_fifo_full;
  assign fifo_data_in = {signals_trig_store, signals_store, timestamp_store};
  
  ts_fifo ts_fifo_inst (
    .rst(rst),        // input wire rst
    .wr_clk(clk_ts),  // input wire wr_clk
    .rd_clk(clk_cfg),  // input wire rd_clk
    .din(fifo_data_in),        // input wire [31 : 0] din
    .wr_en(store_event),    // input wire wr_en
    .rd_en(read_ts_fifo),    // input wire rd_en
    .dout(ts_fifo_out),      // output wire [31 : 0] dout
    .full(ts_fifo_full),      // output wire full
    .empty(fifo_empty)    // output wire empty
  );
/*  
    ila_4 ila_ts (
        .clk(clk_ts), // input wire clk
        .probe0(conf_capture_enable_Sts), // input wire [0:0]  probe0  
        .probe1(t0), // input wire [0:0]  probe1 
        .probe2(conf_signal_edges_Sts), // input wire [15:0]  probe2 
        .probe3(timestamp_store), // input wire [47:0]  probe3 
        .probe4(signals_store), // input wire [7:0]  probe4 
        .probe5(store_event), // input wire [0:0]  probe5 
        .probe6(fifo_empty), // input wire [0:0]  probe6 
        .probe7(signals_in), // input wire [7:0]  probe7
        .probe8(signals_trig_store) // input wire [7:0]  probe
    );
*/  
endmodule