`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Create Date: 13.11.2018 14:30:00
// Design Name: Pulser
// Module Name: pulse_generator
// Project Name: 
// Target Devices: CaR board
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// signal clock domain crossing 
module pulse_generator (
    input  clk,
    input  rst,
    input  pulser_start, // 1 will load parameters and start pulser
    input  pulser_stop, // 1 will stop the pulser but keep the pulse_count output
    input  idle_state, // Set to 1 will keep the idle output state in 1, 0 otherwise. 
    input  trigger, //  
    input  trigger_enable, //  
    input  trigger_edge, //  
    input  [31 : 0] periods,
    input  [31 : 0] time_active,
    input  [31 : 0] time_idle,
    output pulse_out,
    output pulser_running,
    output [31 : 0] pulse_count
    );

    // output signals
    logic pulser_running_int;

    logic start_now;
    //logic pulser_start_prev;
    logic load_regs;
    logic pulse_active;
    // period counter
    logic [31:0] periods_counter, periods_reg;
    logic periods_zero, periods_count, periods_reset;
    // time registers
    logic [31:0] time_counter, time_active_reg, time_idle_reg;
    logic time_zero, time_count, time_active_set, time_idle_set;

    logic trigger_prev, trigger_active;

    typedef enum {
        s_idle,
        s_start,
        s_active,
        s_inactive
    } t_State;
    t_State State, NextState;

    assign start_now = pulser_start || trigger_active;
    assign pulse_out = idle_state ^ pulse_active;
    assign pulser_running = pulser_running_int;
    assign pulse_count = periods_counter;


    always_ff @(posedge clk) begin
        trigger_prev <= trigger;
    end
    assign trigger_active = trigger_enable && ((trigger_edge ^^ (!trigger_prev)) && (trigger_edge ^^ trigger));

    // load parameters into registers:
    always_ff @(posedge clk) begin
        if (rst) begin
            periods_reg <= 0;
            time_active_reg <= 0;
            time_idle_reg <= 0;
        end
        else if (load_regs) begin
            periods_reg <= periods;
            time_active_reg <= time_active;
            time_idle_reg <= time_idle;
        end
        else begin
            periods_reg <= periods_reg;
            time_active_reg <= time_active_reg;
            time_idle_reg <= time_idle_reg;
        end
    end
    
    // periods counter
    always_ff @(posedge clk) begin
        periods_counter <= periods_counter;
        if (periods_reset || rst) begin
            periods_counter <= 0;
        end
        else if (periods_count && !periods_zero) begin
            periods_counter <= periods_counter + 1;
        end
    end
    // status signal when period counting reached the limit
    always_comb begin
        if (periods_counter == periods_reg)
            periods_zero = 1'b1;
        else
            periods_zero = 1'b0;
    end

    // time counter
    always_ff @(posedge clk) begin
        if (rst) 
            time_counter <= 0;
        else begin
            time_counter <= time_counter;
            // load active time
            if (time_active_set) begin
                time_counter <= time_active_reg;
            end
            // load idle time
            if (time_idle_set) begin
                time_counter <= time_idle_reg;
            end
            // count
            else if (time_count && !time_zero) begin
                time_counter <= time_counter - 1;
            end
        end
    end
    assign time_zero = !(|time_counter);
    
    // pulser_start signal rising edge detection
    /*
    always_ff @(posedge clk) begin
        pulser_start_prev <= pulser_start;
    end
    assign start_now = pulser_start && (!pulser_start_prev);
    */

    // FSM transition    
    always_ff @(posedge clk) begin
        if (rst)
            State <= s_idle;
        else
            State <= NextState;
    end

    // Next state logic
    always_comb begin
        // default is to stay in current state
        NextState = State;
        // pulser_stop and start_now transitions apply in a same way to all states: 
        if (pulser_stop)
            NextState = s_idle;
        else if (start_now) 
            NextState = s_start;
        else begin
            // state-specific transitions
            case (State)
                s_idle:
                    NextState = s_idle; 

                s_start:
                    if (periods_zero)
                        NextState = s_idle; 
                    else
                        NextState = s_inactive;

                s_inactive:
                    if (time_zero)
                        NextState = s_active;
                    else
                        NextState = s_inactive;
                
                s_active:
                    if (time_zero) begin
                        if (periods_zero)
                            NextState = s_idle;
                        else
                            NextState = s_inactive;
                    end
                    else
                        NextState = s_active;

                default:
                    NextState = s_idle;
            endcase
        end
    end

    // output logic
    always_comb begin
        // default output values:
        load_regs = 1'b0;
        periods_reset = 1'b0;
        periods_count = 1'b0;
        time_active_set = 1'b0;
        time_idle_set = 1'b0;
        time_count = 1'b0;
        pulse_active = 1'b0;
        pulser_running_int = 1'b0;
        case (State)
            s_idle: 
                if (start_now && !pulser_stop) begin
                    load_regs = 1'b1;
                    periods_reset = 1'b1;
                end
            
            s_start:
                if (!pulser_stop) begin
                    if (start_now) begin
                        load_regs = 1'b1;
                        periods_reset = 1'b1;
                    end
                    else begin
                        time_idle_set = 1'b1;
                        pulser_running_int = 1'b1;
                    end
                end

            s_inactive:
                if (start_now) begin
                    load_regs = 1'b1;
                    periods_reset = 1'b1;
                end
                else begin
                    pulser_running_int = 1'b1;
                    if (time_zero) begin
                        time_active_set = 1'b1;
                        periods_count = 1'b1;
                    end
                    else begin
                        time_count = 1'b1;
                    end
                end

            s_active:
                if (start_now) begin 
                    load_regs = 1'b1;
                    periods_reset = 1'b1;
                end
                else begin
                    pulser_running_int = 1'b1;
                    pulse_active = 1'b1;
                    if (time_zero) begin
                        time_idle_set = 1'b1;
                    end
                    else begin
                        time_count = 1'b1;
                    end
                end

            //default:
            //begin
            //end
        endcase
  end
/*
  ila_pulser ila_pulser_inst (
    .clk(clk), // input wire clk
    .probe0(pulse_active),       // input wire [0:0]  probe0  
    .probe1(pulser_running_int), // input wire [0:0]  probe1 
    .probe2(start_now),          // input wire [0:0]  probe2 
    .probe3(periods_counter),    // input wire [31:0]  probe3 
    .probe4(periods_reg),        // input wire [31:0]  probe4 
    .probe5(time_counter),       // input wire [31:0]  probe5 
    .probe6(time_active_reg),    // input wire [31:0]  probe6 
    .probe7(time_idle_reg),      // input wire [31:0]  probe7
    .probe8(load_regs),          // input wire [0:0]  probe8
    .probe9(periods_reset),      // input wire [0:0]  probe9
    .probe10(periods_count),     // input wire [0:0]  probe10
    .probe11(time_active_set),   // input wire [0:0]  probe11
    .probe12(time_idle_set),     // input wire [0:0]  probe12
    .probe13(time_count),        // input wire [0:0]  probe13
    .probe14(time_zero)          // input wire [0:0]  probe14
  );
*/
endmodule
