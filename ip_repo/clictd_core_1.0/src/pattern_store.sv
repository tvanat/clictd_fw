`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.09.2019 15:26:15
// Design Name: 
// Module Name: pattern_store
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pattern_store #(
    int unsigned PATTERN_MEM_DEPTH = 32
  )(
    input clk,
    input rst,
    
    output mem_write_strobe,
    output [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_write_address,
    output [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_last_address,
    
    input timer_notzero,
    input conf_write_pattern,

    output reset_generator,
    output write_default,
    output mem_empty
  );


  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] store_mem_ptr;
  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_last_ptr;
  logic rst_mem_ptr;
  logic pattern_mem_write;
  logic reset_generator_int;
  logic mem_empty_i;

  assign mem_write_strobe  = pattern_mem_write;
  assign mem_write_address = store_mem_ptr;
  assign mem_last_address  = mem_last_ptr;
  assign reset_generator = reset_generator_int; 
  assign mem_empty = mem_empty_i;
  
  assign write_default = conf_write_pattern && !timer_notzero;
  assign rst_mem_ptr = (conf_write_pattern && !timer_notzero) || rst;
  assign pattern_mem_write = conf_write_pattern && timer_notzero && |mem_last_ptr;

  // pattern mem STORE counter
  always_ff @(posedge clk) begin
    if (rst_mem_ptr) begin
      store_mem_ptr <= PATTERN_MEM_DEPTH-1;
      mem_last_ptr  <= PATTERN_MEM_DEPTH-1;
      reset_generator_int <= 1'b1;
      mem_empty_i <= 1'b1;
    end
    else if (pattern_mem_write && |store_mem_ptr) begin
      store_mem_ptr <= store_mem_ptr - 1;
      mem_last_ptr  <= store_mem_ptr;
      reset_generator_int <= 1'b1; 
      mem_empty_i <= 1'b0;
    end
    else begin
      store_mem_ptr <= store_mem_ptr;
      mem_last_ptr  <= mem_last_ptr;
      reset_generator_int <= 1'b0;
      mem_empty_i <= mem_empty;
    end
  end




endmodule
