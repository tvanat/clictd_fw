`timescale 1ns / 1ps
//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: SignalSyncerBus.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  12.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//  03.04.2018  2        T. Vanat   <tomas.vanat@cern.ch>  rewritten for systemverilog submodules, changed port names
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      Synchronizes bus signal in CDC, based on bus change (no strobe signal).
//  Only for spare changes. For a too quick burst of changes, not all intermediate values will be
//  propagated. The last change will always be propagated.
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

module sync_bus #(
    int WIDTH  = 1,
    int STAGES = 2
) (
    input in_clk,
    input out_clk,
    input  [WIDTH-1:0] bus_in,
    output [WIDTH-1:0] bus_out
);

    logic [WIDTH-1:0] bus_in_stored = 0;
    logic [WIDTH-1:0] bus_out_sig   = 0;
    logic captureInput; 
    logic inputChanged;
    logic propagateChange_id, propagateChange_od;  
    logic changePropagated_id, changePropagated_od = 0;

    typedef enum {
        s_awaitingChange,
        s_changeDetected // Detected change on incoming data, waiting for the acknowledge 
    } t_State;

    t_State State   = s_awaitingChange;
    assign bus_out  = bus_out_sig;
    
    always_ff @(posedge in_clk)
        if (captureInput)
            bus_in_stored <= bus_in;
    //
    assign inputChanged = |(bus_in_stored ^ bus_in);

  
    always_ff @(posedge in_clk) begin
        captureInput       <= 1;
        propagateChange_id <= 0;
        State              <= State;
        
        case(State)
            s_awaitingChange: begin
                if (inputChanged) begin
                    State              <= s_changeDetected;
                    captureInput       <= 0;
                    propagateChange_id <= 1;
                end
            end
            s_changeDetected: begin
                captureInput <= 0;
                if (changePropagated_id) begin
                    State        <= s_awaitingChange;
                    captureInput <= 1;
                end
            end
        endcase
        
    end
    
    
    always_ff @(posedge out_clk) begin
        changePropagated_od <= 0;
        if (propagateChange_od) begin
            bus_out_sig         <= bus_in_stored;
            changePropagated_od <= 1;
        end
    end
    
    
    sync_pulse #(
        .WIDTH(1),
        .STAGES(STAGES)
    ) i_propagateChange_sync (
        .in_clk(in_clk),
        .out_clk(out_clk),
        .pulse_in(propagateChange_id),
        .pulse_out(propagateChange_od)
    );

    sync_pulse #(
        .WIDTH(1),
        .STAGES(STAGES)
    ) i_changePropagated_sync (
        .in_clk(out_clk),
        .out_clk(in_clk),
        .pulse_in(changePropagated_od),
        .pulse_out(changePropagated_id)
    );


endmodule