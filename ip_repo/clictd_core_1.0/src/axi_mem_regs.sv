`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2019 10:47:28
// Design Name: 
// Module Name: axi_mem_regs
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axi_mem_regs #(
    bit AXI_64BIT_DATA = 0,
    byte unsigned AXI_REGISTER_N = 20,
  
    localparam byte unsigned REG_ADDR_BITS  = $clog2(AXI_REGISTER_N),
    localparam byte unsigned AXI_ADDR_LSB   = AXI_64BIT_DATA ? 3 : 2,
    localparam byte unsigned AXI_DATA_WIDTH = AXI_64BIT_DATA ? 64 : 32,
    localparam byte unsigned AXI_DATA_BYTES = AXI_64BIT_DATA ? 8 : 4,
    localparam byte unsigned AXI_ADDR_WIDTH = REG_ADDR_BITS + AXI_ADDR_LSB
  )(
    // REG INTERFACE:
    output [AXI_DATA_WIDTH-1:0] dout,
    output [AXI_DATA_BYTES-1:0] wrByteStrobe,
    output                      wrSelect [AXI_REGISTER_N-1:0],
    output                      rdStrobe [AXI_REGISTER_N-1:0],
    input  [AXI_DATA_WIDTH-1:0] din      [AXI_REGISTER_N-1:0],

		// AXI BUS INTERFACE:
    input S_AXI_ACLK,
		input S_AXI_ARESETN,
		input [AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		input [2 : 0] S_AXI_AWPROT,
		input S_AXI_AWVALID,
		output S_AXI_AWREADY,
		input [AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		input [AXI_DATA_BYTES-1 : 0] S_AXI_WSTRB,
		input S_AXI_WVALID,
		output S_AXI_WREADY,
		output [1 : 0] S_AXI_BRESP,
		output S_AXI_BVALID,
		input S_AXI_BREADY,
		input [AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		input [2 : 0] S_AXI_ARPROT,
		input S_AXI_ARVALID,
		output S_AXI_ARREADY,
		output [AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		output [1 : 0] S_AXI_RRESP,
		output S_AXI_RVALID,
		input S_AXI_RREADY
	);

  logic [AXI_DATA_WIDTH-1:0] axi_mem_din;
  logic [REG_ADDR_BITS-1:0]  axi_mem_rdAddr;
  logic [REG_ADDR_BITS-1:0]  axi_mem_wrAddr;
  //logic axi_mem_rdEnable;
  logic axi_mem_rdStrobe;
  logic rdSelect [AXI_REGISTER_N-1:0];
  logic [AXI_DATA_BYTES-1:0] wrByteStrobe_i;
  logic wrSelect_i [AXI_REGISTER_N-1:0];
  logic [AXI_DATA_WIDTH-1:0] dout_i;
  
  assign dout = dout_i;
  assign wrSelect = wrSelect_i;
  assign wrByteStrobe = wrByteStrobe_i;
  
  generate
    for (genvar i=0; i<AXI_REGISTER_N; i=i+1) begin
      assign rdStrobe[i] = rdSelect[i] && axi_mem_rdStrobe;
    end
  endgenerate

  axi_mem_interface #(
    .AXI_64BIT_DATA(AXI_64BIT_DATA),
    .REG_ADDR_BITS (REG_ADDR_BITS )
  ) axi_interface_inst (
    // MEMORY:
    .axi_mem_din         (axi_mem_din),
    .axi_mem_dout        (dout_i),
    .axi_mem_rdAddr      (axi_mem_rdAddr),
    .axi_mem_wrAddr      (axi_mem_wrAddr),
    .axi_mem_wrByteStrobe(wrByteStrobe_i),
    //.axi_mem_rdEnable    (axi_mem_rdEnable),
    .axi_mem_rdStrobe    (axi_mem_rdStrobe),
		// AXI BUS:
    .S_AXI_ACLK    (S_AXI_ACLK ),
		.S_AXI_ARESETN (S_AXI_ARESETN),
		.S_AXI_AWADDR  (S_AXI_AWADDR ),
		.S_AXI_AWPROT  (S_AXI_AWPROT ),
		.S_AXI_AWVALID (S_AXI_AWVALID),
		.S_AXI_AWREADY (S_AXI_AWREADY),
		.S_AXI_WDATA   (S_AXI_WDATA  ),
		.S_AXI_WSTRB   (S_AXI_WSTRB  ),
		.S_AXI_WVALID  (S_AXI_WVALID ),
		.S_AXI_WREADY  (S_AXI_WREADY ),
		.S_AXI_BRESP   (S_AXI_BRESP  ),
		.S_AXI_BVALID  (S_AXI_BVALID ),
		.S_AXI_BREADY  (S_AXI_BREADY ),
		.S_AXI_ARADDR  (S_AXI_ARADDR ),
		.S_AXI_ARPROT  (S_AXI_ARPROT ),
		.S_AXI_ARVALID (S_AXI_ARVALID),
		.S_AXI_ARREADY (S_AXI_ARREADY),
		.S_AXI_RDATA   (S_AXI_RDATA  ),
		.S_AXI_RRESP   (S_AXI_RRESP  ),
		.S_AXI_RVALID  (S_AXI_RVALID ),
		.S_AXI_RREADY  (S_AXI_RREADY )
	);


  reg_demux #(
    .DATA_WIDTH     (AXI_DATA_WIDTH),
    .AXI_REGISTER_N (AXI_REGISTER_N)
  ) demux_inst (
    .busData     (axi_mem_din),
    .rdAddr      (axi_mem_rdAddr),
    .wrAddr      (axi_mem_wrAddr),
    .regRdSelect (rdSelect),
    .regWrSelect (wrSelect_i),
    .regDin      (din)
  );
/*
  ila_2 ila_axi (
    .clk(S_AXI_ACLK), // input wire clk
    .probe0({2'b00,axi_mem_rdAddr}), // input wire [5:0]  probe0  
    .probe1({2'b00,axi_mem_wrAddr}), // input wire [5:0]  probe1 
    .probe2(wrByteStrobe_i), // input wire [3:0]  probe2 
    .probe3({rdSelect[11],rdSelect[10],rdSelect[9],rdSelect[8],rdSelect[7],rdSelect[6],rdSelect[5],rdSelect[4],rdSelect[3],rdSelect[2],rdSelect[1],rdSelect[0]}), // input wire [11:0]  probe3 
    .probe4({wrSelect_i[11],wrSelect_i[10],wrSelect_i[9],wrSelect_i[8],wrSelect_i[7],wrSelect_i[6],wrSelect_i[5],wrSelect_i[4],wrSelect_i[3],wrSelect_i[2],wrSelect_i[1],wrSelect_i[0]}), // input wire [11:0]  probe4 
    .probe5(axi_mem_rdStrobe), // input wire [0:0]  probe5 
    .probe6(dout_i), // input wire [31:0]  probe6
    .probe7(axi_mem_din), // input wire [31:0]  probe7
    
    .probe8(din[0]), // input wire [31:0]  probe8
    .probe9(din[1]), // input wire [31:0]  probe9
    .probe10(din[6]), // input wire [31:0]  probe10
    .probe11(din[7]), // input wire [31:0]  probe11
    .probe12(din[8]), // input wire [31:0]  probe12
    .probe13(din[9]), // input wire [31:0]  probe13
    .probe14(din[10]), // input wire [31:0]  probe14 
    .probe15(din[11]) // input wire [31:0]  probe15
  );
*/
endmodule
