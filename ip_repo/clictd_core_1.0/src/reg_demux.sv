
`timescale 1 ns / 1 ps

module reg_demux #(
    byte unsigned DATA_WIDTH = 32,
    byte unsigned AXI_REGISTER_N = 3,
  
    localparam byte unsigned ADDR_WIDTH = $clog2(AXI_REGISTER_N)
    //localparam byte unsigned AXI_REGISTER_N = 2**ADDR_WIDTH
  )(
    output [DATA_WIDTH-1:0] busData,
    input  [ADDR_WIDTH-1:0] rdAddr,
    input  [ADDR_WIDTH-1:0] wrAddr,
    
    output                  regRdSelect [AXI_REGISTER_N-1:0],
    output                  regWrSelect [AXI_REGISTER_N-1:0],
    input  [DATA_WIDTH-1:0] regDin      [AXI_REGISTER_N-1:0]
  );

  logic [DATA_WIDTH-1:0] din_i;
  logic regRdSelect_i [AXI_REGISTER_N-1:0];
  logic regWrSelect_i [AXI_REGISTER_N-1:0];

  assign busData = din_i;

  always_comb begin
    if (rdAddr < AXI_REGISTER_N)
      din_i = regDin[rdAddr];
    else 
      din_i = 'b0; 
  end

  generate
    for (genvar i=0; i<AXI_REGISTER_N; i=i+1) begin : demux
      always_comb begin : rd
        if (rdAddr == i)
          regRdSelect_i[i] = 1'b1;
        else
          regRdSelect_i[i] = 1'b0;
      end :rd
      assign regRdSelect[i] = regRdSelect_i[i];
       
      always_comb begin : wr
        if (wrAddr == i)
          regWrSelect_i[i] = 1'b1;
        else
          regWrSelect_i[i] = 1'b0;
      end :wr
      assign regWrSelect[i] = regWrSelect_i[i];
      
    end : demux
  endgenerate

endmodule