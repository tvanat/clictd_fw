`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2019 20:35:19
// Design Name: 
// Module Name: fw_clictd
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fw_clictd #(
    bit AXI_64BIT_DATA = 0,
    int AXI_REGISTER_N = 20,

    localparam byte REG_ADDR_BITS  = $clog2(AXI_REGISTER_N),
    localparam byte AXI_ADDR_LSB   = AXI_64BIT_DATA ? 3 : 2,
    localparam byte AXI_DATA_WIDTH = AXI_64BIT_DATA ? 64 : 32,
    localparam byte AXI_DATA_BYTES = AXI_64BIT_DATA ? 8 : 4,
    localparam byte AXI_ADDR_WIDTH = REG_ADDR_BITS + AXI_ADDR_LSB
  )(
    input clk_100,
    input ext_trigger,
    input tlu_trigger,
    input t0,
    // CHIP INTERFACE:
    input  chip_clk,
    input  chip_data,
    input  chip_enable,
    output chip_readout,
    output chip_pwren,
    output chip_tpulse,
    output chip_shutter,
    output chip_rstn,
    // end CHIP INTERFACE
    // AXI BUS INTERFACE:
    input S_AXI_ACLK,
    input S_AXI_ARESETN,
    input [AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
    input [2 : 0] S_AXI_AWPROT,
    input S_AXI_AWVALID,
    output S_AXI_AWREADY,
    input [AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
    input [AXI_DATA_BYTES-1 : 0] S_AXI_WSTRB,
    input S_AXI_WVALID,
    output S_AXI_WREADY,
    output [1 : 0] S_AXI_BRESP,
    output S_AXI_BVALID,
    input S_AXI_BREADY,
    input [AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
    input [2 : 0] S_AXI_ARPROT,
    input S_AXI_ARVALID,
    output S_AXI_ARREADY,
    output [AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
    output [1 : 0] S_AXI_RRESP,
    output S_AXI_RVALID,
    input S_AXI_RREADY
    // end AXI BUS INTERFACE
  );

  localparam byte unsigned PG_PATTERN_MEM_DEPTH = 32;
  localparam byte unsigned PG_RUN_COUNT_WIDTH = 32;
  localparam byte unsigned PG_TIMER_WIDTH = 32;
  localparam byte unsigned PG_SIGNALS_N = 8;
  localparam byte unsigned PG_TRIGGERS_N = 5;

  logic chip_readout_i;
  logic [1:0]chip_pwren_enable;
  logic chip_pwren_i;
  logic chip_pwren_i1;
  logic chip_pwren_i2;
  logic [2:0]chip_tpulse_enable;
  logic chip_tpulse_i;
  logic chip_tpulse_i1;
  logic chip_tpulse_i2;
  logic chip_tpulse_i3;
  logic [2:0]chip_shutter_enable;
  logic chip_shutter_i;
  logic chip_shutter_i1;
  logic chip_shutter_i2;
  logic chip_shutter_i3;
  logic [1:0]chip_rst_enable;
  logic chip_rst_i;
  logic chip_rst_i1;
  logic chip_rst_i2;
  
  logic t0_S100;
  logic ext_trigger_S100;
  logic tlu_trigger_S100;
  logic chip_readout_S100;

  logic [(PG_TRIGGERS_N)-1 : 0] wave_triggers_in;
  logic [PG_SIGNALS_N-1 : 0] wave_outputs_out;
  logic [PG_TIMER_WIDTH-1 : 0] timer_pattern_conf;
  logic [PG_SIGNALS_N-1 : 0] output_pattern_conf;
  logic [(PG_TRIGGERS_N*3)+2 : 0] triggers_pattern_conf;
  logic conf_write_pattern_in;
  logic [PG_RUN_COUNT_WIDTH-1 : 0] conf_runs_n;    
  logic [($clog2(PG_PATTERN_MEM_DEPTH+1))-1 : 0] remaining_pattern_capacity;    
  logic conf_run_terminate;
  logic conf_run_start;
  logic stat_gen_running;
  logic gen_rst;
  
  logic [47:0] ts_conf_ts_init_value; 
  logic [15:0] ts_conf_signal_edges; 
  logic [63:0] ts_fifo_data; 
  //logic [15:0] ts_signal_in; 
  logic [7:0] ts_signal_in_S100; 
  logic ts_fifo_read; 
  logic ts_fifo_msb_read; 
  logic ts_fifo_msb_read_latch; 
  logic ts_fifo_lsb_read;
  logic ts_fifo_lsb_read_latch;
  logic ts_fifo_status_read;
  logic ts_fifo_full;
  logic ts_fifo_full_latch;
  logic ts_fifo_data_valid;
  logic ts_conf_enable;
  logic ts_conf_capture_enable;
  
  logic pulser_start;
  logic pulser_stop;
  logic pulser_start_s100;
  logic pulser_start_w;
  logic pulser_start_w_prev;
  logic pulser_start_w_pulse;
  logic pulser_stop_s100;
  logic pulser_rst;
  logic pulser_active;
  logic [31:0] pulser_periods;
  logic [31:0] pulser_time_active;
  logic [31:0] pulser_time_idle;
  logic [31:0] pulser_periods_s100;
  logic [31:0] pulser_time_active_s100;
  logic [31:0] pulser_time_idle_s100;

  logic chip_rst_sync_axi;
  logic chip_clk_rise;
  logic chip_clk_fall;
  logic chip_clk_off;
  logic chip_clk_rise_reg;
  logic chip_clk_fall_reg;
  logic chip_clk_miss_reg;
  
  logic [63 : 0] fifo_data_out;
  logic fifo_data_valid;
  logic fifo_data_read_msb;
  logic fifo_data_read_lsb;
  logic readout_start;
  logic readout_start_a;
  //logic readout_start_a_Schip;
  logic readout_start_w;
  logic readout_start_w_prev;
  logic readout_start_w_pulse;
  logic readout_start_w_pulse_Saxi;
  //logic readout_start_w_Schip;
  //logic readout_start_w_prev;
  //logic readout_start_w_pulse;
  logic readout_reset;
  logic readout_active_S100;
  logic readout_active_Saxi;
  logic readout_busy_S100;
  logic readout_busy_Saxi;
  logic readout_end;
  logic [15:0] readout_frame_bitcount;
  logic rd_shutterstart;
  logic [AXI_DATA_WIDTH-1:0] rd_shuttertimeout;
  
  logic [AXI_DATA_WIDTH-1:0] ifr_dout;
  logic [AXI_DATA_BYTES-1:0] ifr_wrByteStrobe;
  logic                      ifr_wrSelect [AXI_REGISTER_N-1:0];
  logic                      ifr_rdStrobe [AXI_REGISTER_N-1:0];
  logic [AXI_DATA_WIDTH-1:0] ifr_din      [AXI_REGISTER_N-1:0];  
  
  /////////////////////
  // OUTPUTS TO CHIP // 
  /////////////////////

  assign chip_readout   = chip_readout_i;
  assign chip_pwren     = chip_pwren_i;
  assign chip_tpulse    = chip_tpulse_i;
  assign chip_shutter   = chip_shutter_i;
  assign chip_rstn      = ~chip_rst_i;
  
  //assign chip_pwren_i   = chip_pwren_i1 || chip_pwren_i2;
  //assign chip_tpulse_i  = chip_tpulse_i1 | chip_tpulse_i2;
  //assign chip_shutter_i = chip_shutter_i1 || chip_shutter_i2 || chip_shutter_i3;
  //assign chip_rst_i     = chip_rst_i1 || chip_rst_i2;

  output_enable_or #(
    .INPUTS_N(2)
  ) chipout_pwren (
    .signal_out(chip_pwren_i),
    .conf_input_enable(chip_pwren_enable),
    .signals_in({chip_pwren_i2, chip_pwren_i1})
  );

  output_enable_or #(
    .INPUTS_N(3)
  ) chipout_tpulse (
    .signal_out(chip_tpulse_i),
    .conf_input_enable(chip_tpulse_enable),
    .signals_in({chip_tpulse_i3, chip_tpulse_i2, chip_tpulse_i1})
  );

  output_enable_or #(
    .INPUTS_N(3)
  ) chipout_shutter (
    .signal_out(chip_shutter_i),
    .conf_input_enable(chip_shutter_enable),
    .signals_in({chip_shutter_i3, chip_shutter_i2, chip_shutter_i1})
  );

  output_enable_or #(
    .INPUTS_N(2)
  ) chipout_rst (
    .signal_out(chip_rst_i),
    .conf_input_enable(chip_rst_enable),
    .signals_in({chip_rst_i2, chip_rst_i1})
  );


  /////////////////////
  // AXI BUS         // 
  /////////////////////

  axi_mem_regs #(
    .AXI_64BIT_DATA (AXI_64BIT_DATA),
    .AXI_REGISTER_N (AXI_REGISTER_N)
  ) axi_inst (
    // INTERFACE REGISTERS:
    .dout(ifr_dout),
    .wrByteStrobe(ifr_wrByteStrobe),
    .wrSelect(ifr_wrSelect),
    .rdStrobe(ifr_rdStrobe),
    .din(ifr_din),
    // AXI BUS:
    .S_AXI_ACLK    (S_AXI_ACLK ),
    .S_AXI_ARESETN (S_AXI_ARESETN),
    .S_AXI_AWADDR  (S_AXI_AWADDR ),
    .S_AXI_AWPROT  (S_AXI_AWPROT ),
    .S_AXI_AWVALID (S_AXI_AWVALID),
    .S_AXI_AWREADY (S_AXI_AWREADY),
    .S_AXI_WDATA   (S_AXI_WDATA  ),
    .S_AXI_WSTRB   (S_AXI_WSTRB  ),
    .S_AXI_WVALID  (S_AXI_WVALID ),
    .S_AXI_WREADY  (S_AXI_WREADY ),
    .S_AXI_BRESP   (S_AXI_BRESP  ),
    .S_AXI_BVALID  (S_AXI_BVALID ),
    .S_AXI_BREADY  (S_AXI_BREADY ),
    .S_AXI_ARADDR  (S_AXI_ARADDR ),
    .S_AXI_ARPROT  (S_AXI_ARPROT ),
    .S_AXI_ARVALID (S_AXI_ARVALID),
    .S_AXI_ARREADY (S_AXI_ARREADY),
    .S_AXI_RDATA   (S_AXI_RDATA  ),
    .S_AXI_RRESP   (S_AXI_RRESP  ),
    .S_AXI_RVALID  (S_AXI_RVALID ),
    .S_AXI_RREADY  (S_AXI_RREADY )
  );
  
  ///////////////////////
  // PATTERN GENERATOR //
  ///////////////////////

  sync_signal #(
      .WIDTH(6),
      .STAGES(2)
  ) sync_wg_trig (
      .out_clk(clk_100),
      .signal_in ({t0,      ext_trigger,      tlu_trigger,      chip_readout_i   , readout_active_Saxi, readout_busy_Saxi}),
      .signal_out({t0_S100, ext_trigger_S100, tlu_trigger_S100, chip_readout_S100, readout_active_S100, readout_busy_S100})
  );

  assign wave_triggers_in = {pulser_active, readout_active_S100, readout_busy_S100, tlu_trigger_S100, ext_trigger_S100};
  assign readout_start_w = wave_outputs_out[0];
  assign chip_pwren_i1   = wave_outputs_out[1];
  assign chip_tpulse_i1  = wave_outputs_out[2];
  assign chip_shutter_i1 = wave_outputs_out[3];
  assign chip_rst_i1     = wave_outputs_out[4];
  assign pulser_start_w  = wave_outputs_out[5];

  pattern_generator #(
      .PG_PATTERN_MEM_DEPTH(PG_PATTERN_MEM_DEPTH),
      .PG_RUN_COUNT_WIDTH(PG_RUN_COUNT_WIDTH),
      .PG_TIMER_WIDTH(PG_TIMER_WIDTH),
      .PG_SIGNALS_N(PG_SIGNALS_N),
      .PG_TRIGGERS_N(PG_TRIGGERS_N)
    )(
      .clk_cfg(S_AXI_ACLK),
      .clk_gen(clk_100),
      .rst((~S_AXI_ARESETN) || gen_rst),
  
      .triggers_in(wave_triggers_in),
      .outputs_out(wave_outputs_out),
      .mem_remaining_capacity(remaining_pattern_capacity),
      .timer_pattern_conf_in(timer_pattern_conf),
      .output_pattern_conf_in(output_pattern_conf),
      .triggers_pattern_conf_in(triggers_pattern_conf),
      .conf_write_pattern_in(conf_write_pattern_in),
      .conf_runs_n(conf_runs_n),
      .conf_run_terminate(conf_run_terminate),
      .conf_run_start(conf_run_start),
      .stat_gen_running(stat_gen_running)
    );
    
  /////////////////////////
  // CHIP CLOCK WATCHDOG //
  ///////////////////////// 
  
  always @( posedge S_AXI_ACLK ) begin
    if ( S_AXI_ARESETN == 1'b0 ) begin
        chip_clk_rise_reg  <= 1'b0;
        chip_clk_fall_reg  <= 1'b0;
        chip_clk_miss_reg  <= 1'b0;
    end 
    else begin
        if (ifr_rdStrobe[1]) begin
          chip_clk_rise_reg  <= 1'b0;
          chip_clk_fall_reg  <= 1'b0;
          chip_clk_miss_reg  <= 1'b0;
        end 

        if (chip_clk_rise)
          chip_clk_rise_reg  <= 1'b1; 
        if (chip_clk_fall)
          chip_clk_fall_reg  <= 1'b1; 
        if (chip_clk_off)
          chip_clk_miss_reg  <= 1'b1;

      end
  end
  
  clkcheck clkcheck_inst(
    .clk40(chip_clk),
    .clk250(S_AXI_ACLK),
    .arst((~S_AXI_ARESETN) || chip_rst_i),
    .rise(chip_clk_rise),
    .fall(chip_clk_fall),
    .noclk(chip_clk_off)
  );



  ///////////////////////
  // PULSER            //
  ///////////////////////

  sync_reset #(
    .NEGATIVE(1),
    .STAGES(2) 
  ) sync_rst_gen (
    .clk(clk_100),
    .arst_in(S_AXI_ARESETN),  // i, async 
    .rst_out(pulser_rst)   // o, sync to clk
  );
  
  sync_bus #(
      .WIDTH(32),
      .STAGES(2)
  ) sync_pulser_periods (
      .in_clk(S_AXI_ACLK),
      .out_clk(clk_100),
      .bus_in(pulser_periods),
      .bus_out(pulser_periods_s100)
  );
  
  sync_bus #(
      .WIDTH(32),
      .STAGES(2)
  ) sync_pulser_time_active (
      .in_clk(S_AXI_ACLK),
      .out_clk(clk_100),
      .bus_in(pulser_time_active),
      .bus_out(pulser_time_active_s100)
  );
  
  sync_bus #(
      .WIDTH(32),
      .STAGES(2)
  ) sync_pulser_time_idle (
      .in_clk(S_AXI_ACLK),
      .out_clk(clk_100),
      .bus_in(pulser_time_idle),
      .bus_out(pulser_time_idle_s100)
  );

  
  sync_pulse #(
      .WIDTH(2),
      .STAGES(2)
  ) pulse_sync_pulser_cfg (
      .in_clk(S_AXI_ACLK),
      .out_clk(clk_100),
      .pulse_in({pulser_start, pulser_stop}),
      .pulse_out({pulser_start_s100, pulser_stop_s100})
  );
  
  always_ff @(posedge clk_100)
    pulser_start_w_prev <= pulser_start_w; 
   
  assign pulser_start_w_pulse = !pulser_start_w_prev && pulser_start_w;  
  
  
  pulse_generator pulse_generator_inst (
      .clk(clk_100),
      .rst(pulser_rst),
      .pulser_start(pulser_start_s100 || pulser_start_w_pulse),
      .pulser_stop(pulser_stop_s100),
      .trigger(1'b0), //  
      .trigger_enable(1'b0), //  
      .trigger_edge(1'b0), //  
      .idle_state(1'b0), 
      .periods(pulser_periods_s100),
      .time_active(pulser_time_active_s100),
      .time_idle(pulser_time_idle_s100),
      .pulse_out(chip_tpulse_i3),
      .pulser_running(pulser_active),
      .pulse_count()
  );


  ///////////////////////
  // READOUT           //
  ///////////////////////
  
  
  assign readout_start = readout_start_a || readout_start_w_pulse_Saxi;
  
  always_ff @(posedge clk_100)
    readout_start_w_prev <= readout_start_w;
  
  assign readout_start_w_pulse = !readout_start_w_prev && readout_start_w;
  
  sync_pulse #(
      .WIDTH(1),
      .STAGES(2)
  ) pulse_sync_rd_start (
      .in_clk(clk_100),
      .out_clk(S_AXI_ACLK),
      .pulse_in(readout_start_w_pulse),
      .pulse_out(readout_start_w_pulse_Saxi)
  );
  
  readout_merge readout_merge_inst (
    .clk_100(clk_100),
    .clk_axi(S_AXI_ACLK),
    .arst((~S_AXI_ARESETN) || chip_clk_off || readout_reset),
    
    // FIFO output
    .fifo_data_out(fifo_data_out),
    .fifo_data_valid(fifo_data_valid),
    .fifo_data_read_msb(fifo_data_read_msb),
    .fifo_data_read_lsb(fifo_data_read_lsb),
    .readout_busy(readout_busy_Saxi),
    .readout_active(readout_active_Saxi),
    
    .fifo_status_read(1'b0),
    
    // timestamp inputs - sync to clk_100
    .t0(t0_S100),
    .ts_signal_in({ext_trigger_S100, chip_readout_S100, tlu_trigger_S100, chip_pwren, chip_tpulse, chip_shutter, readout_busy_S100, t0_S100}),

    // timestamps control and config
    // internally synced to clk_100
    .ts_conf_enable(ts_conf_enable),
    .ts_conf_capture_enable(ts_conf_capture_enable),
    .ts_conf_ts_init_value(ts_conf_ts_init_value),
    .ts_conf_signal_edges(ts_conf_signal_edges),

    // 1 pulse sync to clk_axi
    .readout_start(readout_start),
    
    // CHIP INTERFACE:
    .chip_clk(chip_clk),
    .chip_data(chip_data),
    .chip_enable(chip_enable),
    .chip_readout(chip_readout_i)
    // end CHIP INTERFACE

  );
  
  
/*
  
  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_t0 (
      .out_clk(clk_100),
      .signal_in(t0),
      .signal_out(t0_S100)
  );
  
  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_chip_readout_S100 (
      .out_clk(clk_100),
      .signal_in(chip_readout_i),
      .signal_out(chip_readout_S100)
  );
  
  assign ts_fifo_read = (ts_fifo_msb_read_latch || ts_fifo_msb_read) && (ts_fifo_lsb_read_latch || ts_fifo_lsb_read);
   
  always_ff @(posedge S_AXI_ACLK) begin
    if ((~S_AXI_ARESETN) || chip_clk_off || readout_reset || ts_fifo_status_read || ts_fifo_read) begin
      ts_fifo_msb_read_latch <= 1'b0;
      ts_fifo_lsb_read_latch <= 1'b0;
    end 
    else begin
      if (ts_fifo_lsb_read) begin 
        ts_fifo_lsb_read_latch <= 1'b1;
      end
      if (ts_fifo_msb_read) begin 
        ts_fifo_msb_read_latch <= 1'b1;
      end
    end
  end 
  
  always_ff @(posedge S_AXI_ACLK) begin
    if ((~S_AXI_ARESETN) || chip_clk_off || readout_reset || ts_fifo_status_read) begin
      ts_fifo_full_latch <= 1'b0;
    end 
    else if (ts_fifo_full) begin 
      ts_fifo_full_latch <= 1'b1;
    end
  end 
  
  assign ts_signal_in_S100 = {ext_trigger_S100, chip_readout_S100, tlu_trigger_S100, chip_pwren_i1, chip_tpulse_i1, chip_shutter_i1, 1'b0, t0_S100};
  
  timestamps #(
    .TS_WIDTH(48),
    .TS_SIGNALS_N(8)
  ) timestamps_inst (
    .clk_ts(clk_100),
    .clk_cfg(S_AXI_ACLK),
    .rst((~S_AXI_ARESETN) || chip_clk_off || readout_reset),
    .t0(t0_S100),
    .conf_enable(ts_conf_enable),
    .conf_capture_enable(ts_conf_capture_enable),
    .conf_ts_init_value(ts_conf_ts_init_value),
    .conf_signal_edges(ts_conf_signal_edges),
    .signals_in(ts_signal_in_S100),
    .read_ts_fifo(ts_fifo_read),
    .ts_fifo_out(ts_fifo_data),
    .ts_fifo_full(ts_fifo_full),
    .output_valid(ts_fifo_data_valid)
  ); 

  ///////////////////////
  // CHIP DATA READOUT //
  /////////////////////// 

  assign readout_start = readout_start_a_Schip || readout_start_w_Schip;
  
  always_ff @(posedge clk_100)
    readout_start_w_prev <= readout_start_w;
    
  assign readout_start_w_pulse = readout_start_w && !readout_start_w_prev;
    
  sync_pulse #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_readout_start_axi (
      .in_clk(S_AXI_ACLK),
      .out_clk(chip_clk),
      .pulse_in(readout_start_a),
      .pulse_out(readout_start_a_Schip)
  );    
  
  sync_pulse #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_readout_start_wave (
      .in_clk(clk_100),
      .out_clk(chip_clk),
      .pulse_in(readout_start_w_pulse),
      .pulse_out(readout_start_w_Schip)
  );

  clictd_readout #(
    .OUTPUT_BYTES(AXI_DATA_BYTES)
  ) clictd_readout_inst (
    // clk clock domain
    .fifo_clk(S_AXI_ACLK),
    .arst((~S_AXI_ARESETN) || chip_clk_off || readout_reset),
    .fifo_data_out(fifo_data_out), // o [(OUTPUT_BYTES*8)-1 : 0]
    .fifo_data_out_valid(fifo_data_out_valid), // o
    .fifo_data_out_read(fifo_data_out_read),
    
    // chip_clk clock domain    
    .frame_bitcount(readout_frame_bitcount), // o [15:0] 
    .data_readout_end(readout_end), // o
    .readout_active(readout_active), // o
    .readout_start(readout_start),
    .chip_clk(chip_clk),
    .chip_data(chip_data),
    .chip_enable(chip_enable),
    .chip_readout(chip_readout_i)
  );
*/

  /////////////////////////
  // AXI REGS ASSIGNMENT //
  /////////////////////////

  // 0
  assign ifr_din[0] = 'b0;
  //assign fifo_data_out_read = ifr_rdStrobe[0];
  // 1
  assign ifr_din[1] = {24'b0, readout_busy_Saxi, chip_shutter_i3, readout_active_Saxi, chip_clk_off, chip_clk_miss_reg, chip_clk_fall_reg, chip_clk_rise_reg, 1'b0};
  // 2
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      readout_start_a <= 1'b0;
      readout_reset   <= 1'b0;
      rd_shutterstart <= 1'b0;    
    end 
    else begin
      readout_start_a <= 1'b0;
      readout_reset   <= 1'b0;
      rd_shutterstart <= 1'b0;
      if (ifr_wrSelect[2])
          if ( ifr_wrByteStrobe[0] == 1 ) begin
            readout_start_a <= ifr_dout[0];
            readout_reset   <= ifr_dout[1];
            rd_shutterstart <= ifr_dout[2];
          end
    end
  end
  assign ifr_din[2] = 32'b0;
  //3
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      chip_pwren_i2   <= 1'b0;
      chip_tpulse_i2  <= 1'b0;
      chip_shutter_i2 <= 1'b0;
      chip_rst_i2     <= 1'b0;  
    end 
    else begin
      if (ifr_wrSelect[3])
          if ( ifr_wrByteStrobe[0] == 1 ) begin
            chip_pwren_i2   <= ifr_dout[2];
            chip_tpulse_i2  <= ifr_dout[3];
            chip_shutter_i2 <= ifr_dout[4];
            chip_rst_i2     <= ifr_dout[5];
          end
      end
  end
  assign ifr_din[3] = {24'b0, 2'b0, chip_rst_i2, chip_shutter_i2, chip_tpulse_i2, chip_pwren_i2, 2'b0};
  // 4
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      rd_shuttertimeout <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[4])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            rd_shuttertimeout[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[4] = rd_shuttertimeout;
  // 5
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      conf_run_start        <= 1'b0;
      conf_run_terminate    <= 1'b0;
      gen_rst               <= 1'b0;
      conf_write_pattern_in <= 1'b0;
    end 
    else begin
      conf_run_start        <= 1'b0;
      conf_run_terminate    <= 1'b0;
      gen_rst               <= 1'b0;
      conf_write_pattern_in <= 1'b0;
      if (ifr_wrSelect[5])
          if ( ifr_wrByteStrobe[0] == 1 ) begin
            conf_run_start        <= ifr_dout[0]; // start pattern generator
            conf_run_terminate    <= ifr_dout[1]; // stop generator after finishing the current run
            gen_rst               <= ifr_dout[2]; // stop immediatelly (reset)
            conf_write_pattern_in <= ifr_dout[3]; // push pattern into memory          
          end
    end
  end
  assign ifr_din[5] = {32'b0};
  // 6
  assign ifr_din[6] = {31'b0, stat_gen_running};
  // 7
  assign ifr_din[7] = remaining_pattern_capacity;
  // 8
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      conf_runs_n <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[8])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            conf_runs_n[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[8] = conf_runs_n;
  // 9
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      timer_pattern_conf <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[9])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            timer_pattern_conf[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[9] = timer_pattern_conf;
  // 10
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      output_pattern_conf <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[10]) begin
        if ( ifr_wrByteStrobe[0] == 1 )
          output_pattern_conf[7:0] <= ifr_dout[7:0];
        //if ( ifr_wrByteStrobe[1] == 1 )
        //  output_pattern_conf[15:8] <= ifr_dout[15:8];
      end
    end
  end
  assign ifr_din[10] = {24'b0, output_pattern_conf};
  // 11
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      triggers_pattern_conf <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[11]) begin
          if ( ifr_wrByteStrobe[0] == 1 )
            triggers_pattern_conf[7:0] <= ifr_dout[7:0];
          if ( ifr_wrByteStrobe[1] == 1 )
            triggers_pattern_conf[14:8] <= ifr_dout[14:8];
      end
    end
  end
  assign ifr_din[11] = {20'b0, triggers_pattern_conf};
  // 12
  assign fifo_data_read_lsb = ifr_rdStrobe[12];
  assign ifr_din[12] = fifo_data_out[31:0];
  // 13
  assign fifo_data_read_msb = ifr_rdStrobe[13];
  assign ifr_din[13] = fifo_data_out[63:32];
  // 14
  //assign ts_fifo_status_read = ifr_rdStrobe[14];
  assign ifr_din[14] = {31'b0, fifo_data_valid};
  /*
  // 12
  assign ts_fifo_lsb_read = ifr_rdStrobe[12];
  assign ifr_din[12] = ts_fifo_data[31:0];
  // 13
  assign ts_fifo_msb_read = ifr_rdStrobe[13];
  assign ifr_din[13] = ts_fifo_data[63:32];
  // 14
  assign ts_fifo_status_read = ifr_rdStrobe[14];
  assign ifr_din[14] = {30'b0, ts_fifo_full_latch, ts_fifo_data_valid};
  */
  // 15
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      ts_conf_enable         <= 1'b0;
      ts_conf_capture_enable <= 1'b0;
    end 
    else begin
      if (ifr_wrSelect[15])
          if ( ifr_wrByteStrobe[0] == 1 ) begin
            ts_conf_enable         <= ifr_dout[0]; // start pattern generator
            ts_conf_capture_enable <= ifr_dout[1]; // stop generator after finishing the current run
          end
    end
  end
  assign ifr_din[15] = {30'b0, ts_conf_capture_enable, ts_conf_enable};
  // 16
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      ts_conf_signal_edges <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[16])
        for (byte unsigned byte_index = 0; byte_index <= 1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            ts_conf_signal_edges[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[16] = {16'b0, ts_conf_signal_edges};
  // 17
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      ts_conf_ts_init_value[31:0] <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[17])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            ts_conf_ts_init_value[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[17] = ts_conf_ts_init_value[31:0];
  // 18
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      ts_conf_ts_init_value[47:32] <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[18])
          for (byte unsigned byte_index = 0; byte_index <= 1; byte_index = byte_index+1 )
            if ( ifr_wrByteStrobe[byte_index] == 1 )
              ts_conf_ts_init_value[((byte_index*8)+32) +: 8] <= ifr_dout[(byte_index*8) +: 8];
      end
  end
  assign ifr_din[18] = {16'b0, ts_conf_ts_init_value[47:32]};
  // 19
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      chip_pwren_enable   <= 2'b11;
      chip_tpulse_enable  <= 3'b111;
      chip_shutter_enable <= 3'b111;
      chip_rst_enable     <= 2'b11;
    end
    else begin 
      if (ifr_wrSelect[19]) begin
        if ( ifr_wrByteStrobe[0] == 1 )
           chip_pwren_enable   <= ifr_dout[1:0];
        if ( ifr_wrByteStrobe[1] == 1 )
           chip_tpulse_enable  <= ifr_dout[10:8];
        if ( ifr_wrByteStrobe[2] == 1 )
           chip_shutter_enable <= ifr_dout[18:16];
        if ( ifr_wrByteStrobe[3] == 1 )
           chip_rst_enable     <= ifr_dout[25:24];
      end          
    end
  end
  assign ifr_din[19] = {6'b0, chip_rst_enable, 5'b0, chip_shutter_enable, 5'b0, chip_tpulse_enable, 6'b0, chip_pwren_enable};
  // 20
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      pulser_periods[31:0] <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[20])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            pulser_periods[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[20] = pulser_periods[31:0];
  // 21
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      pulser_time_active[31:0] <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[21])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            pulser_time_active[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[21] = pulser_time_active[31:0];
  // 22
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      pulser_time_idle[31:0] <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[22])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            pulser_time_idle[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[22] = pulser_time_idle[31:0];
  // 23
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      pulser_start <= 1'b0;
      pulser_stop  <= 1'b0;
    end 
    else begin
      pulser_start <= 1'b0;
      pulser_stop  <= 1'b0;
      if (ifr_wrSelect[23])
        if ( ifr_wrByteStrobe[0] == 1 ) begin
          pulser_start <= ifr_dout[0];
          pulser_stop  <= ifr_dout[1];
        end
    end
  end
  assign ifr_din[23] = 32'b0;

/*
  // xx
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      _FIXME_[31:0] <= 'b0;
    end 
    else begin
      if (ifr_wrSelect[_FIXME_])
        for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
          if ( ifr_wrByteStrobe[byte_index] == 1 )
            _FIXME_[(byte_index*8) +: 8] <= ifr_dout[(byte_index*8) +: 8];
    end
  end
  assign ifr_din[_FIXME_] = _FIXME_[31:0]; 
*/
  
   /* 
      ila_5 ila_ts_axiregs (
        .clk(S_AXI_ACLK), // input wire clk
        .probe0(ts_fifo_data), // input wire [63:0]  probe0  
        .probe1(ts_fifo_lsb_read), // input wire [0:0]  probe1 
        .probe2(ts_fifo_msb_read), // input wire [0:0]  probe2 
        .probe3(ts_fifo_status_read), // input wire [0:0]  probe3 
        .probe4(ts_fifo_data_valid), // input wire [0:0]  probe4 
        .probe5(ts_fifo_full_latch), // input wire [0:0]  probe5 
        .probe6(ts_fifo_read), // input wire [0:0]  probe6 
        .probe7(ts_fifo_lsb_read_latch), // input wire [0:0]  probe7
        .probe8(ts_fifo_msb_read_latch), // input wire [0:0]  probe
        .probe9(ts_fifo_full), // input wire [0:0]  probe
        .probe10(ts_signal_in[6:0]) // input wire [6:0]  probe
    );
  */
  
        // Instantiation of shutter timeout
  shuttercontrol shuttercontrol_inst (
    .clk(S_AXI_ACLK),
    .reset(readout_reset || !S_AXI_ARESETN),
    .start(rd_shutterstart),
    .timeout(rd_shuttertimeout),
    .shutter(chip_shutter_i3)
  );
  
endmodule
