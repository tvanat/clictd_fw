
`timescale 1 ns / 1 ps

module output_enable_or #(
    byte unsigned INPUTS_N = 3
  )(
    output signal_out,
    input  [INPUTS_N-1:0] conf_input_enable,
    input  [INPUTS_N-1:0] signals_in
  );

  logic signal_out_int;
  logic [INPUTS_N-1:0] signals_in_enable;

  assign signal_out = signal_out_int;

  assign signals_in_enable = signals_in & conf_input_enable;

  assign signal_out_int = |signals_in_enable;

endmodule