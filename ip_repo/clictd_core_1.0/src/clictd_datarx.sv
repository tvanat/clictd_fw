`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 09:48:04
// Design Name: 
// Module Name: caribou_clictd_datarx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
// 
// 
//////////////////////////////////////////////////////////////////////////////////


module clictd_datarx (
  input rst,
  input chip_clk,
  input chip_data,
  input chip_enable,
  input readout_start,
  
  output chip_readout,
  output readout_end,
  output readout_idle,
  output [15 : 0] frame_bitcount,
  output [7 : 0] data_out,
  output data_valid
);

  logic [2 : 0] rd_byte_bitcount;
  logic [15 : 0] rd_frame_bitcount;
  logic [7 : 0] shreg_data;
  logic [7 : 0] shreg_data_aligned;
  
  logic [7 : 0] data_out_o;
  logic data_valid_o;
  logic chip_readout_o;
  
  logic rd_bitcount_en;
  logic rd_bitcount_rst;
  logic rd_byte_store;
  logic readout_end_o;
  logic readout_idle_o;

  typedef enum	{IDLE, START, ENABLE, FINISH} STATE_READOUT;
  STATE_READOUT rd_state, rd_state_next;

  assign data_out   = data_out_o;
  assign data_valid = data_valid_o;
  assign chip_readout = chip_readout_o;
  assign readout_end  = readout_end_o;
  assign readout_idle = readout_idle_o;
  assign frame_bitcount = rd_frame_bitcount;
  
  // readout FSM state transition
  always_ff @(posedge chip_clk)
    if (rst)
      rd_state <= IDLE;
    else 
      rd_state <= rd_state_next;

   // readout FSM next state logic
  always_comb begin   
    // default is to stay in current state
    rd_state_next = rd_state;
    case (rd_state)
      IDLE : begin
        if (readout_start)
          rd_state_next = START;
        else
          rd_state_next = IDLE;
      end
      START : begin
        if (chip_enable)
          rd_state_next = ENABLE;
        else
          rd_state_next = START;
      end
      ENABLE : begin
        if (chip_enable)
          rd_state_next = ENABLE;
        else
          rd_state_next = FINISH;
      end
      FINISH : begin
        if (readout_start)
          rd_state_next = FINISH;
        else
          rd_state_next = IDLE;
      end
      default : begin
        rd_state_next = IDLE;
      end
    endcase  
  end
  
  // readout FSM outputs
  always_comb begin
    // defaults
    chip_readout_o  = 1'b0;
    rd_bitcount_en  = 1'b0;
    rd_bitcount_rst = 1'b0;
    rd_byte_store   = 1'b0;
    readout_end_o   = 1'b0;
    readout_idle_o  = 1'b0;

    case (rd_state)
      IDLE : begin
        chip_readout_o  = 1'b0;
        rd_bitcount_en  = 1'b0;
        rd_byte_store   = 1'b0;
        readout_end_o   = 1'b0;
        if (readout_start) begin
          rd_bitcount_rst = 1'b1;
          readout_idle_o  = 1'b0;
        end
        else begin
          rd_bitcount_rst = 1'b0;
          readout_idle_o  = 1'b1;
        end
      end

      START : begin
        chip_readout_o  = 1'b1;
        rd_byte_store   = 1'b0;
        readout_end_o   = 1'b0;
        readout_idle_o  = 1'b0;
        if (chip_enable) begin
          rd_bitcount_en  = 1'b1;
          rd_bitcount_rst = 1'b0;
        end
        else begin
          rd_bitcount_en  = 1'b0;
          rd_bitcount_rst = 1'b1;
        end
      end

      ENABLE : begin
        rd_bitcount_rst = 1'b0;
        readout_idle_o  = 1'b0;
        // data is coming
        if (chip_enable) begin
          chip_readout_o  = 1'b1;
          rd_bitcount_en  = 1'b1;
          readout_end_o   = 1'b0;
        end
        // readout finished
        else begin
          chip_readout_o  = 1'b0;
          rd_bitcount_en  = 1'b0;
          readout_end_o   = 1'b1;
        end

        // shreg full or it is the last bit of the readout
        if ((~|rd_byte_bitcount) || !chip_enable)
          rd_byte_store  = 1'b1;
        else
          rd_byte_store  = 1'b0;
      end
      
      FINISH : begin
        readout_idle_o  = 1'b0;
        chip_readout_o  = 1'b0;
        rd_bitcount_rst = 1'b0;
        rd_bitcount_en  = 1'b0;
        rd_byte_store   = 1'b0;
        readout_end_o   = 1'b1;
      end
      
    endcase  

  end

  assign shreg_data_aligned = shreg_data << rd_byte_bitcount;
  
  // input data shift register
  always_ff @(posedge chip_clk) begin
    if (rst || rd_bitcount_rst) begin
      shreg_data <= 'b0;
    end
    else if (rd_bitcount_en) begin
      shreg_data[7 : 1] <= shreg_data[6 : 0];
      shreg_data[0]     <= chip_data;
    end
    else begin
      shreg_data <= shreg_data;
    end
  end
  
  // input byte bit counter
  always_ff @(posedge chip_clk) begin
    if (rst || rd_bitcount_rst) begin
      rd_byte_bitcount <= 0;
    end
    else if (rd_bitcount_en) begin
      rd_byte_bitcount <= rd_byte_bitcount - 1;
    end 
    else begin 
      rd_byte_bitcount <= rd_byte_bitcount;
    end
  end

  // input frame bit counter
  always_ff @(posedge chip_clk) begin
    if (rst || rd_bitcount_rst) begin
      rd_frame_bitcount <= 0;
    end
    else if (rd_bitcount_en) begin
      rd_frame_bitcount <= rd_frame_bitcount + 1;
    end 
    else begin 
      rd_frame_bitcount <= rd_frame_bitcount;
    end
  end
  
    
  // output data register
  always_ff @(posedge chip_clk) begin
    if (rst || rd_bitcount_rst) begin
      data_out_o   <= 'b0;
      data_valid_o <= 1'b0;
    end
    else if (rd_byte_store) begin
      data_out_o   <= shreg_data_aligned; 
      data_valid_o <= 1'b1;
    end
    else begin 
      data_out_o   <= data_out_o;
      data_valid_o <= 1'b0;
    end
  end


endmodule