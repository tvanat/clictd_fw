`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2019 09:48:04
// Design Name: 
// Module Name: caribou_clictd_datarx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
// chip_data and chip_enable are set with rising edge of chip_clk, so they should be read on falling edge of chip_clk.
// 
//////////////////////////////////////////////////////////////////////////////////


module caribou_clictd_datarx (
  input arst,
  input chip_clk,
  input chip_data,
  input chip_enable,

  output [7 : 0] data_out,
  output data_valid,
  output data_frame
);

  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) logic chip_data_sync;
  (* ASYNC_REG = "true", keep = "true", shreg_extract = "no" *) logic chip_enable_sync;
  logic rst;
  logic [2 : 0] readout_bitcount;
  logic [7 : 0] shreg_data;
  logic [7 : 0] shreg_data_aligned;
  logic [7 : 0] output_data;
  logic output_valid;
  logic output_valid_next;
  logic readout_bitcount_start;
  logic readout_bitcount_zero;
  logic readout_inprogress;
  logic readout_inprogress_next;

  // Synchronise reset to chip_clk
  sync_reset #(
      .NEGATIVE(1'b0),
      .STAGES(2) 
  ) datarx_rst (
      .clk(chip_clk),
      .arst_in(arst),  // i, async 
      .rst_out(rst)   // o, sync to clk, active 1
  );
  
  // capture input synchronously to chip_clk
  always_ff @(negedge chip_clk) begin
    chip_data_sync   <= chip_data;
    chip_enable_sync <= chip_enable;
  end

  // readout FSM state transition
  always_ff @(negedge chip_clk)
    if (rst)
      readout_inprogress <= 1'b0;
    else 
      readout_inprogress <= readout_inprogress_next;

   // readout FSM next state logic
  always_comb begin   
    if (!readout_inprogress) begin
      if(chip_enable_sync)
        readout_inprogress_next = 1'b1;
      else 
        readout_inprogress_next = 1'b0;
    end
    else begin
      if (chip_enable_sync)
        readout_inprogress_next = 1'b1;
      else
        readout_inprogress_next = 1'b0;
    end
  end
  
  // readout FSM outputs
  always_comb begin
    if (!readout_inprogress) begin
      if(chip_enable_sync)
        readout_bitcount_start = 1'b1;
      else 
        readout_bitcount_start = 1'b0;
    end
    else begin
      if (chip_enable_sync)
        readout_bitcount_start = 1'b0;
      else
        readout_bitcount_start = 1'b0;
    end
  end
  
  // input data shift register
  always_ff @(negedge chip_clk) begin
    shreg_data[7 : 1] <= shreg_data[6 : 0];
    shreg_data[0]     <= chip_data_sync;
  end
  
  // input data bit counter
  always_ff @(negedge chip_clk) begin
    if (rst || readout_bitcount_start) begin
      readout_bitcount <= 3'b111;
    end
    else begin
      readout_bitcount <= readout_bitcount - 1;
    end 
  end
  
  assign readout_bitcount_zero = ~|readout_bitcount;
  assign shreg_data_aligned = shreg_data << readout_bitcount;
    
  // output data register
  always_ff @(negedge chip_clk) begin
    if (rst) begin
      output_data <= 'b0;
      output_valid_next <= 1'b0;
    end
    else if (readout_inprogress && (readout_bitcount_zero || !readout_inprogress_next)) begin
      output_data <= shreg_data_aligned; 
    else 
      output_data <= output_data;
  end


endmodule  