//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
//Date        : Fri Nov 29 16:39:46 2019
//Host        : pclcd36 running 64-bit major release  (build 9200)
//Command     : generate_target caribou_clictd_top_wrapper.bd
//Design      : caribou_clictd_top_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module caribou_clictd_top_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    FMC_REFCLK_N,
    FMC_REFCLK_P,
    FMC_TLU_CLK_N,
    FMC_TLU_CLK_P,
    chip_clk_dbg,
    chip_clk_n,
    chip_clk_p,
    chip_data_dbg,
    chip_data_n,
    chip_data_p,
    chip_enable_dbg,
    chip_enable_n,
    chip_enable_p,
    chip_pwren_dbg,
    chip_pwren_n,
    chip_pwren_p,
    chip_readout_dbg,
    chip_readout_n,
    chip_readout_p,
    chip_rstn_dbg,
    chip_rstn_n,
    chip_rstn_p,
    chip_shutter_dbg,
    chip_shutter_n,
    chip_shutter_p,
    chip_tpulse_dbg,
    chip_tpulse_n,
    chip_tpulse_p,
    clk_100_n,
    clk_100_p,
    ext_trig,
    sys_diff_clock_clk_n,
    sys_diff_clock_clk_p,
    t0_n,
    t0_p,
    tlu_trig_n,
    tlu_trig_p);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  output FMC_REFCLK_N;
  output FMC_REFCLK_P;
  input FMC_TLU_CLK_N;
  input FMC_TLU_CLK_P;
  output chip_clk_dbg;
  input chip_clk_n;
  input chip_clk_p;
  output chip_data_dbg;
  input chip_data_n;
  input chip_data_p;
  output chip_enable_dbg;
  input chip_enable_n;
  input chip_enable_p;
  output chip_pwren_dbg;
  output chip_pwren_n;
  output chip_pwren_p;
  output chip_readout_dbg;
  output chip_readout_n;
  output chip_readout_p;
  output chip_rstn_dbg;
  output chip_rstn_n;
  output chip_rstn_p;
  output chip_shutter_dbg;
  output chip_shutter_n;
  output chip_shutter_p;
  output chip_tpulse_dbg;
  output chip_tpulse_n;
  output chip_tpulse_p;
  input clk_100_n;
  input clk_100_p;
  input ext_trig;
  input sys_diff_clock_clk_n;
  input sys_diff_clock_clk_p;
  input t0_n;
  input t0_p;
  input tlu_trig_n;
  input tlu_trig_p;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire FMC_REFCLK_N;
  wire FMC_REFCLK_P;
  wire FMC_TLU_CLK_N;
  wire FMC_TLU_CLK_P;
  wire chip_clk_dbg;
  wire chip_clk_n;
  wire chip_clk_p;
  wire chip_data_dbg;
  wire chip_data_n;
  wire chip_data_p;
  wire chip_enable_dbg;
  wire chip_enable_n;
  wire chip_enable_p;
  wire chip_pwren_dbg;
  wire chip_pwren_n;
  wire chip_pwren_p;
  wire chip_readout_dbg;
  wire chip_readout_n;
  wire chip_readout_p;
  wire chip_rstn_dbg;
  wire chip_rstn_n;
  wire chip_rstn_p;
  wire chip_shutter_dbg;
  wire chip_shutter_n;
  wire chip_shutter_p;
  wire chip_tpulse_dbg;
  wire chip_tpulse_n;
  wire chip_tpulse_p;
  wire clk_100_n;
  wire clk_100_p;
  wire ext_trig;
  wire sys_diff_clock_clk_n;
  wire sys_diff_clock_clk_p;
  wire t0_n;
  wire t0_p;
  wire tlu_trig_n;
  wire tlu_trig_p;

  caribou_clictd_top caribou_clictd_top_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .FMC_REFCLK_N(FMC_REFCLK_N),
        .FMC_REFCLK_P(FMC_REFCLK_P),
        .FMC_TLU_CLK_N(FMC_TLU_CLK_N),
        .FMC_TLU_CLK_P(FMC_TLU_CLK_P),
        .chip_clk_dbg(chip_clk_dbg),
        .chip_clk_n(chip_clk_n),
        .chip_clk_p(chip_clk_p),
        .chip_data_dbg(chip_data_dbg),
        .chip_data_n(chip_data_n),
        .chip_data_p(chip_data_p),
        .chip_enable_dbg(chip_enable_dbg),
        .chip_enable_n(chip_enable_n),
        .chip_enable_p(chip_enable_p),
        .chip_pwren_dbg(chip_pwren_dbg),
        .chip_pwren_n(chip_pwren_n),
        .chip_pwren_p(chip_pwren_p),
        .chip_readout_dbg(chip_readout_dbg),
        .chip_readout_n(chip_readout_n),
        .chip_readout_p(chip_readout_p),
        .chip_rstn_dbg(chip_rstn_dbg),
        .chip_rstn_n(chip_rstn_n),
        .chip_rstn_p(chip_rstn_p),
        .chip_shutter_dbg(chip_shutter_dbg),
        .chip_shutter_n(chip_shutter_n),
        .chip_shutter_p(chip_shutter_p),
        .chip_tpulse_dbg(chip_tpulse_dbg),
        .chip_tpulse_n(chip_tpulse_n),
        .chip_tpulse_p(chip_tpulse_p),
        .clk_100_n(clk_100_n),
        .clk_100_p(clk_100_p),
        .ext_trig(ext_trig),
        .sys_diff_clock_clk_n(sys_diff_clock_clk_n),
        .sys_diff_clock_clk_p(sys_diff_clock_clk_p),
        .t0_n(t0_n),
        .t0_p(t0_p),
        .tlu_trig_n(tlu_trig_n),
        .tlu_trig_p(tlu_trig_p));
endmodule
