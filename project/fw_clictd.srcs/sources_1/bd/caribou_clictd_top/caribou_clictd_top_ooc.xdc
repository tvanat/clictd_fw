################################################################################

# This XDC is used only for OOC mode of synthesis, implementation
# This constraints file contains default clock frequencies to be used during
# out-of-context flows such as OOC Synthesis and Hierarchical Designs.
# This constraints file is not used in normal top-down synthesis (default flow
# of Vivado)
################################################################################
create_clock -name chip_clk_p -period 25 [get_ports chip_clk_p]
create_clock -name chip_clk_n -period 25 [get_ports chip_clk_n]
create_clock -name clk_100_p -period 10 [get_ports clk_100_p]
create_clock -name clk_100_n -period 10 [get_ports clk_100_n]
create_clock -name FMC_TLU_CLK_P -period 25 [get_ports FMC_TLU_CLK_P]
create_clock -name FMC_TLU_CLK_N -period 25 [get_ports FMC_TLU_CLK_N]
create_clock -name sys_diff_clock_clk_p -period 5 [get_ports sys_diff_clock_clk_p]
create_clock -name processing_system7_FCLK_CLK0 -period 4 [get_pins processing_system7/FCLK_CLK0]

################################################################################