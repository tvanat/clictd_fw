`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 
// Design Name: 
// Module Name: sync_reset
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// reset sync
module sync_reset #(
    logic NEGATIVE = 0,
    int   STAGES   = 2 
)(
    input  clk,
    input  arst_in,  // i, async 
    output rst_out   // o, sync to clk
    );



    (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic reset_sync [STAGES : 0]; // = {{STAGES{0}}};;
    
    logic arst;
    
    //VIVADO does not support assertions... #%*#&$!
    //CHECK_MIN_STAGES: assert (STAGES >= 2) else $error("STAGES must be >= 2");   

    assign arst = arst_in ^ NEGATIVE;
    
    always_ff @(posedge clk, posedge arst) 
    begin
        if (arst)
            reset_sync[0] <= 1'b1;
        else
            reset_sync[0] <= 1'b0;
    end
    
    genvar i;
    generate
    for (i=1; i <= STAGES; i=i+1)
        begin
            always_ff @(posedge clk, posedge arst) 
            begin
                if (arst)
                    reset_sync[i] <= 1'b1;
                else
                    reset_sync[i] <= reset_sync[i-1];
            end
        end
    endgenerate

    assign rst_out = reset_sync[STAGES];
    
endmodule
