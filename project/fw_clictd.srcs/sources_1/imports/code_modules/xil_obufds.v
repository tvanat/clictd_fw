`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.03.2018 10:59:13
// Design Name: 
// Module Name: xil_obufds
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module xil_obufds # (
    parameter IOSTANDARD = "DEFAULT",
    parameter SLEW       = "SLOW"
)
(
    input  I,
    output O_p,
    output O_n
    );
/*    
    parameter IOSTANDARD = "DEFAULT";
    parameter SLEW       = "SLOW";
*/

    wire OB;
    wire O;

    assign O_p = O;
    assign O_n = OB;
   
   OBUFDS #(
      .IOSTANDARD(IOSTANDARD), // Specify the output I/O standard
      .SLEW(SLEW)              // Specify the output slew rate
   ) OBUFDS_inst (
      .O(O),     // Diff_p output (connect directly to top-level port)
      .OB(OB),   // Diff_n output (connect directly to top-level port)
      .I(I)      // Buffer input 
   );

					
					


endmodule
